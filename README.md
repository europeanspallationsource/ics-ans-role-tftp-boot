ics-ans-role-tftp-boot
======================

Ansible role to setup an EEE TFTP boot server.

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

```yaml
tftp_boot_uefi_menuentries:
  - name: "CentOS 7.1.1503 Default (RT) Netboot"
    kernel: "images/mtcacpu-default/vmlinuz-3.10.0-229.7.2.rt56.141.6.el7.centos.x86_64"
    options: "root=nfs:{{ ansible_default_ipv4.address }}:/export/nfsroots/centos7/rootfs,async,timeo=600 ip=dhcp ro selinux=0"
    initrd: "images/mtcacpu-default/initramfs-3.10.0-229.7.2.rt56.141.6.el7.centos.x86_64-nfs.img"
  - name: "CentOS 7.1.1503 Non-RT Netboot"
    kernel: "images/mtcacpu-default/vmlinuz-3.10.0-229.7.2.el7.x86_64"
    options: "root=nfs4:{{ ansible_default_ipv4.address }}:/export/nfsroots/centos7/rootfs,timeo=14,async ip=dhcp selinux=0 ro"
    initrd: "images/mtcacpu-default/initramfs-3.10.0-229.7.2.el7.x86_64-nfs.img"
  - name: "CentOS 7.1.1503 Installer"
    kernel: "boot/images/pxeboot/vmlinuz"
    options: "inst.repo=http://vault.centos.org/7.1.1503/os/x86_64 inst.lang=en_US ip=dhcp"
    initrd: "boot/images/pxeboot/initrd.img"

tftp_boot_pxelinux_default: centos7rt
tftp_boot_pxelinux_menuentries:
  - label: centos7rt
    name: "Centos 7.1.1503 (RT)"
    kernel: "../images/mtcacpu-default/vmlinuz-3.10.0-229.7.2.rt56.141.6.el7.centos.x86_64"
    initrd: "../images/mtcacpu-default/initramfs-3.10.0-229.7.2.rt56.141.6.el7.centos.x86_64-nfs.img"
    options: "root=nfs:{{ ansible_default_ipv4.address }}:/export/nfsroots/centos7/rootfs,async,timeo=14 ip=dhcp rw selinux=0"
  - label: centos7nonrt
    name: "Centos 7.1.1503 (non RT)"
    kernel: "../images/mtcacpu-default/vmlinuz-3.10.0-229.7.2.el7.x86_64"
    initrd: "../images/mtcacpu-default/initramfs-3.10.0-229.7.2.el7.x86_64-nfs.img"
    options: "root=nfs:{{ ansible_default_ipv4.address }}:/export/nfsroots/centos7/rootfs,async,timeo=14 ip=dhcp rw selinux=0"
  - label: centos7installer
    name: "CentOS 7.1.1503 Installer"
    kernel: "images/pxeboot/vmlinuz"
    initrd: "images/pxeboot/initrd.img"
    options: "inst.repo=http://vault.centos.org/7.1.1503/os/x86_64 inst.lang=en_US ip=dhcp"
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-tftp-boot
```

License
-------

BSD 2-clause
